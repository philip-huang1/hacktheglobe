from csp import Constraint, Variable, CSP
from constraints import *
from backtracking import bt_search
import math
import numpy as np
import util


##################################################################
# Scheduling
##################################################################

'''Helpers'''
def latlon2dist(x, y):
    # x and y are tuples of (lat, lon)
    # Returns the Euclidean distance (in km) between these two points
    return 6371 * math.sqrt((y[0]-x[0])**2 + math.cos((x[0]+y[0])/2)**2 * (y[1]-x[1])**2)

def simplify_return(list):
    # Returns lists of assignments in desired format
    # [('Region A', 7:00-10:00), ('Region B', 10:00-20:00) ... ]
    ret = []
    past_time = 0
    for i in range(len(list)):
        if i == 0:
            continue
        if list[i] != list[i-1]:
            ret.append((list[i-1], '{0}:00-{1}:00'.format(past_time+7, i+7)))
            past_time = i
        elif i == len(list)-1:
            ret.append((list[i], '{0}:00-{1}:00'.format(past_time+7, i+8)))
    return ret


class ScheduleProblem:

    def __init__(self, regions_dict, weather_dict, min_times_dict, max_flying_dist, max_days):
        # regions is a dictionary with strings of regions as keys and (lat, lon) as values
        # weather_dict is a dictionary with names of timeslots as keys and regions with bad weather
        # ------> If a region has bad weather, flight is not possible
        # ------> This dictionary is updated every T_MAX days, where T_MAX is defined
        # min_times is a dictionary with names of regions as keys and an integer specifying the minimum time that the drone must visit in a day for this region as values
        # max_flying_dist (in km) is a number that specifies the maximum Euclidean distance for drone flight at a time
        # max_days is a number that specifies the number of days for the CSP

        #assign variables
        self.regions = regions_dict
        self.weather = weather_dict
        self.min_times = min_times_dict
        self.MAX_DIST = max_flying_dist
        self.MAX_DAYS = max_days


def drone_schedule(schedule_problem, algo, variableHeuristic='mrv', 
                 allsolns=False, silent=False, trace=False):

    # Abbreviation
    sp = schedule_problem

    # BUILD CSP here and store it in the variable csp
    vars = []
    dom = []

    for region in sp.regions.keys():
        dom.append(region)

    for i in range(sp.MAX_DAYS):
        for j in range((23-7+1)):  # 7 and 23 are user defined
            vars.append(Variable('Timeslot {0}_{1}'.format(j, i), dom))

    cons = []
    # NValuesConstraint for each region specifying the minimum time that the drone must be in each region for each MAX_DAYS
    for region, min_time in sp.min_times.items():
        cons.append(NValuesConstraint('C({} visited)'.format(region), vars, [region], min_time, 23-7+1))

    # TablesConstraint for every two consecutive timeslots specifying:
        # 1. Euclidean distance between the variable value should be <= MAX_DIST
        # 2. If two timeslots have different values, and (if weather[val1] is bad or weather[val2] is bad), don’t include.
    for i in range(len(vars)-1):
        var1 = vars[i]
        var2 = vars[i+1]
        timeslot1 = var1.name()
        timeslot2 = var2.name()
        satisfyingAssignments = []

        for region1, loc1 in sp.regions.items():
            for region2, loc2 in sp.regions.items():
                # print('Dist between {0} and {1} is {2}.'.format(region1, region2, latlon2dist(loc1, loc2)))
                if latlon2dist(loc1, loc2) >= sp.MAX_DIST:
                    continue
                if region1 in sp.weather[timeslot1] or region2 in sp.weather[timeslot2]:
                    continue
                satisfyingAssignments.append([region1, region2])

        cons.append(TableConstraint('C(Timeslots {0} and {1})'.format(timeslot1, timeslot2), [var1, var2], satisfyingAssignments))

    # Set up CSP
    csp = CSP("Scheduling Problem", vars, cons)

    # Invoke search with the passed parameters
    solutions, num_nodes = bt_search(algo, csp, variableHeuristic, allsolns, trace)

    # Find the best solution based on a heuristic: minimize the variance of region_time/min_time
    min_var = float('inf')
    solution = None
    for j in range(len(solutions)):
        assignment = [s[1] for s in solutions[j]]
        count = []
        for i in sp.regions.keys():
            count.append(assignment.count(i)/sp.min_times[i])
        var = np.var(count)
        if var < min_var:
            min_var = var
            solution = assignment

    schedule = []
    for i in range(sp.MAX_DAYS):
        schedule.append(simplify_return(solution[i*(23-7+1):(i+1)*(23-7+1)]))
    return schedule
