import json
import numpy as np
import pandas as pd
from geopy.geocoders import Nominatim
from geopy import distance
from geopy.extra.rate_limiter import RateLimiter


filename = 'Communities without high-capacity transport.xlsx'

df = pd.read_excel(filename, sheet_name='Communities without high-capaci')
df = df.loc[df['Province'] == 'SK']

def search(name, province, geocode):
    place = "{} , {}".format(name, province)
    location = geocode(place)

    if location != None:
        gps = (location.latitude, location.longitude)
        print(place, gps)
        return gps

def save_gps(df):
    geolocator = Nominatim(user_agent="specify_your_app_name_here")
    geocode = RateLimiter(geolocator.geocode, min_delay_seconds = 1)
    gps_pts = []
    places = []
    i = 0


    for index, row in df.iterrows():
        i += 1
        print(i)
        location = search(row['Community name'], row['Province'], geocode)
        if location != None:
            with open("gps.txt", 'a+') as f:
                f.write('{}, {}, {}, {}\n'.format(row['Community name'], row['Province'], location[0], location[1]))
    return

def get_d_matrix():
    with open("gps.txt", 'r') as f:
        lines = f.read().split('\n')

    loc_dict = {}

    for line in lines[:-1]:
        word = line.split(',')
        word[2] = float(word[2])
        word[3] = float(word[3])
        if abs(word[2] - 55) < 15 and abs(word[3] + 105) < 5:
            name = '{},{}'.format(word[0], word[1])
            loc_dict[name] = (word[2], word[3])
            print(name, loc_dict[name])

    with open('gps.json', 'w') as f:
        json.dump(loc_dict, f)
    return


def get_distance_matrix(df):
    gps_pts = []
    places = []
    for index, row in df.iterrows():
        location = search(row['Community name'], row['Province'])
        if location != None:
            gps_pts.append(location)
            places.append((row['Community name'], row['Province']))

    gps_pts = np.array(gps_pts)
    np.save('gps_coord', gps_pts)
    N = len(gps_pts)
    dist_mtx = np.zeros((N, N))

    for i in range(N):
        pt1 = gps_pts[i]
        for j in range(i + 1, N):
            pt2 = gps_pts[j]
            dist = distance.distance(pt1, pt2).km
            dist_mtx[i][j] = dist
            dist_mtx[j][i] = dist

    return dist_mtx

#save_gps(df)
get_d_matrix()
#for index, row in df.iterrows():
#    print(row['Province'])
#    print(row['Community name'])
