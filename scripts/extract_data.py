import numpy as np
from io import StringIO
from sklearn.cluster import DBSCAN

source1 = np.loadtxt("./education.txt", delimiter='\t', dtype=str)
cd = source1[:, 0].astype(np.float)   #census division
edu_category = source1[:, 3].astype(np.float)

source2 = np.loadtxt("./nodiploma.txt", delimiter='\t', dtype=str)
cd = source2[:, 0].astype(np.float)   #census division
no_dip = source2[:, 3].astype(np.float)


if __name__ == '__main__':
    print(cd[np.where(edu_category==5)])
    print(cd[np.where(no_dip == 1)])
    
