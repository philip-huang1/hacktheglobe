1001	Division No. 1, Newfoundland and Labrador	87.6	1
1002	Division No. 2, Newfoundland and Labrador	71.4	4
1003	Division No. 3, Newfoundland and Labrador	66.5	4
1004	Division No. 4, Newfoundland and Labrador	75.0	3
1005	Division No. 5, Newfoundland and Labrador	83.0	2
1006	Division No. 6, Newfoundland and Labrador	80.7	2
1007	Division No. 7, Newfoundland and Labrador	74.5	4
1008	Division No. 8, Newfoundland and Labrador	66.8	4
1009	Division No. 9, Newfoundland and Labrador	71.1	4
1010	Division No. 10, Newfoundland and Labrador	80.0	2
1011	Division No. 11, Newfoundland and Labrador	58.7	5
1101	Kings, Prince Edward Island	81.6	2
1102	Queens, Prince Edward Island	89.8	1
1103	Prince, Prince Edward Island	80.7	2
1201	Shelburne, Nova Scotia	67.1	4
1202	Yarmouth, Nova Scotia	74.0	4
1203	Digby, Nova Scotia	72.2	4
1204	Queens, Nova Scotia	77.1	3
1205	Annapolis, Nova Scotia	76.4	3
1206	Lunenburg, Nova Scotia	79.6	3
1207	Kings, Nova Scotia	84.0	2
1208	Hants, Nova Scotia	81.3	2
1209	Halifax, Nova Scotia	89.5	1
1210	Colchester, Nova Scotia	83.4	2
1211	Cumberland, Nova Scotia	81.6	2
1212	Pictou, Nova Scotia	81.9	2
1213	Guysborough, Nova Scotia	72.0	4
1214	Antigonish, Nova Scotia	88.5	1
1215	Inverness, Nova Scotia	84.3	2
1216	Richmond, Nova Scotia	78.4	3
1217	Cape Breton, Nova Scotia	83.3	2
1218	Victoria, Nova Scotia	78.2	3
1301	Saint John, New Brunswick	86.5	1
1302	Charlotte, New Brunswick	83.9	2
1303	Sunbury, New Brunswick	86.5	1
1304	Queens, New Brunswick	82.8	2
1305	Kings, New Brunswick	91.0	1
1306	Albert, New Brunswick	90.4	1
1307	Westmorland, New Brunswick	87.5	1
1308	Kent, New Brunswick	71.6	4
1309	Northumberland, New Brunswick	80.2	2
1310	York, New Brunswick	91.2	1
1311	Carleton, New Brunswick	85.2	1
1312	Victoria, New Brunswick	81.6	2
1313	Madawaska, New Brunswick	80.6	2
1314	Restigouche, New Brunswick	74.6	4
1315	Gloucester, New Brunswick	70.9	4
2401	Les Îles-de-la-Madeleine, Quebec	73.0	4
2402	Le Rocher-Percé, Quebec	66.8	4
2403	La Côte-de-Gaspé, Quebec	77.8	3
2404	La Haute-Gaspésie, Quebec	64.1	5
2405	Bonaventure, Quebec	78.1	3
2406	Avignon, Quebec	78.5	3
2407	La Matapédia, Quebec	74.3	4
2408	Matane, Quebec	75.5	3
2409	La Mitis, Quebec	75.8	3
2410	Rimouski-Neigette, Quebec	87.3	1
2411	Les Basques, Quebec	75.8	3
2412	Rivière-du-Loup, Quebec	82.0	2
2413	Témiscouata, Quebec	70.4	4
2414	Kamouraska, Quebec	79.6	3
2415	Charlevoix-Est, Quebec	79.0	3
2416	Charlevoix, Quebec	82.0	2
2417	L'Islet, Quebec	73.8	4
2418	Montmagny, Quebec	75.6	3
2419	Bellechasse, Quebec	81.0	2
2420	L'Île-d'Orléans, Quebec	90.2	1
2421	La Côte-de-Beaupré, Quebec	87.7	1
2422	La Jacques-Cartier, Quebec	90.1	1
2423	Québec, Quebec	90.3	1
2425	Lévis, Quebec	90.5	1
2426	La Nouvelle-Beauce, Quebec	84.8	2
2427	Robert-Cliche, Quebec	76.7	3
2428	Les Etchemins, Quebec	72.7	4
2429	Beauce-Sartigan, Quebec	76.3	3
2430	Le Granit, Quebec	68.8	4
2431	Les Appalaches, Quebec	79.0	3
2432	L'Érable, Quebec	75.0	3
2433	Lotbinière, Quebec	78.6	3
2434	Portneuf, Quebec	86.0	1
2435	Mékinac, Quebec	75.6	3
2436	Shawinigan, Quebec	79.7	3
2437	Francheville, Quebec	85.1	1
2438	Bécancour, Quebec	80.6	2
2439	Arthabaska, Quebec	77.3	3
2440	Les Sources, Quebec	71.8	4
2441	Le Haut-Saint-François, Quebec	73.5	4
2442	Le Val-Saint-François, Quebec	80.0	3
2443	Sherbrooke, Quebec	85.8	1
2444	Coaticook, Quebec	75.3	3
2445	Memphrémagog, Quebec	82.5	2
2446	Brome-Missisquoi, Quebec	81.9	2
2447	La Haute-Yamaska, Quebec	79.6	3
2448	Acton, Quebec	71.5	4
2449	Drummond, Quebec	79.1	3
2450	Nicolet-Yamaska, Quebec	76.6	3
2451	Maskinongé, Quebec	74.6	4
2452	D'Autray, Quebec	73.5	4
2453	Pierre-De Saurel, Quebec	77.5	3
2454	Les Maskoutains, Quebec	76.8	3
2455	Rouville, Quebec	78.0	3
2456	Le Haut-Richelieu, Quebec	80.9	2
2457	La Vallée-du-Richelieu, Quebec	89.3	1
2458	Longueuil, Quebec	86.8	1
2459	Marguerite-D'Youville, Quebec	86.4	1
2460	L'Assomption, Quebec	84.6	2
2461	Joliette, Quebec	79.6	3
2462	Matawinie, Quebec	72.6	4
2463	Montcalm, Quebec	69.3	4
2464	Les Moulins, Quebec	84.0	2
2465	Laval, Quebec	85.8	1
2466	Montréal, Quebec	87.7	1
2467	Roussillon, Quebec	86.1	1
2468	Les Jardins-de-Napierville, Quebec	74.1	4
2469	Le Haut-Saint-Laurent, Quebec	74.9	4
2470	Beauharnois-Salaberry, Quebec	77.5	3
2471	Vaudreuil-Soulanges, Quebec	87.7	1
2472	Deux-Montagnes, Quebec	82.8	2
2473	Thérèse-De Blainville, Quebec	86.4	1
2474	Mirabel, Quebec	82.9	2
2475	La Rivière-du-Nord, Quebec	78.8	3
2476	Argenteuil, Quebec	72.0	4
2477	Les Pays-d'en-Haut, Quebec	86.7	1
2478	Les Laurentides, Quebec	77.9	3
2479	Antoine-Labelle, Quebec	69.6	4
2480	Papineau, Quebec	71.8	4
2481	Gatineau, Quebec	84.2	2
2482	Les Collines-de-l'Outaouais, Quebec	83.5	2
2483	La Vallée-de-la-Gatineau, Quebec	68.9	4
2484	Pontiac, Quebec	69.8	4
2485	Témiscamingue, Quebec	72.2	4
2486	Rouyn-Noranda, Quebec	79.7	3
2487	Abitibi-Ouest, Quebec	69.1	4
2488	Abitibi, Quebec	73.6	4
2489	La Vallée-de-l'Or, Quebec	71.8	4
2490	La Tuque, Quebec	67.9	4
2491	Le Domaine-du-Roy, Quebec	77.7	3
2492	Maria-Chapdelaine, Quebec	74.8	4
2493	Lac-Saint-Jean-Est, Quebec	80.1	2
2494	Le Saguenay-et-son-Fjord, Quebec	85.2	1
2495	La Haute-Côte-Nord, Quebec	73.7	4
2496	Manicouagan, Quebec	78.3	3
2497	Sept-Rivières--Caniapiscau, Quebec	75.6	3
2498	Minganie--Le Golfe-du-Saint-Laurent, Quebec	62.4	5
2499	Nord-du-Québec, Quebec	53.9	5
3501	Stormont, Dundas and Glengarry, Ontario	83.2	2
3502	Prescott and Russell, Ontario	86.2	1
3506	Ottawa, Ontario	92.6	1
3507	Leeds and Grenville, Ontario	86.9	1
3509	Lanark, Ontario	87.5	1
3510	Frontenac, Ontario	90.6	1
3511	Lennox and Addington, Ontario	86.5	1
3512	Hastings, Ontario	83.1	2
3513	Prince Edward, Ontario	86.9	1
3514	Northumberland, Ontario	86.4	1
3515	Peterborough, Ontario	87.5	1
3516	Kawartha Lakes, Ontario	84.6	2
3518	Durham, Ontario	89.4	1
3519	York, Ontario	90.6	1
3520	Toronto, Ontario	88.6	1
3521	Peel, Ontario	88.3	1
3522	Dufferin, Ontario	87.6	1
3523	Wellington, Ontario	86.8	1
3524	Halton, Ontario	93.8	1
3525	Hamilton, Ontario	85.7	1
3526	Niagara, Ontario	87.9	1
3528	Haldimand-Norfolk, Ontario	82.4	2
3529	Brant, Ontario	83.9	2
3530	Waterloo, Ontario	86.7	1
3531	Perth, Ontario	82.8	2
3532	Oxford, Ontario	83.2	2
3534	Elgin, Ontario	80.5	2
3536	Chatham-Kent, Ontario	82.0	2
3537	Essex, Ontario	87.2	1
3538	Lambton, Ontario	88.4	1
3539	Middlesex, Ontario	88.4	1
3540	Huron, Ontario	80.2	2
3541	Bruce, Ontario	84.9	2
3542	Grey, Ontario	82.2	2
3543	Simcoe, Ontario	85.5	1
3544	Muskoka, Ontario	85.6	1
3546	Haliburton, Ontario	80.5	2
3547	Renfrew, Ontario	86.4	1
3548	Nipissing, Ontario	84.4	2
3549	Parry Sound, Ontario	83.3	2
3551	Manitoulin, Ontario	80.1	2
3552	Sudbury, Ontario	77.6	3
3553	Greater Sudbury, Ontario	86.5	1
3554	Timiskaming, Ontario	77.9	3
3556	Cochrane, Ontario	78.7	3
3557	Algoma, Ontario	85.1	1
3558	Thunder Bay, Ontario	83.4	2
3559	Rainy River, Ontario	82.0	2
3560	Kenora, Ontario	68.7	4
4601	Division No. 1, Manitoba	76.9	3
4602	Division No. 2, Manitoba	78.2	3
4603	Division No. 3, Manitoba	69.8	4
4604	Division No. 4, Manitoba	77.6	3
4605	Division No. 5, Manitoba	82.8	2
4606	Division No. 6, Manitoba	77.8	3
4607	Division No. 7, Manitoba	84.6	2
4608	Division No. 8, Manitoba	69.0	4
4609	Division No. 9, Manitoba	77.5	3
4610	Division No. 10, Manitoba	89.8	1
4611	Division No. 11, Manitoba	88.1	1
4612	Division No. 12, Manitoba	85.0	1
4613	Division No. 13, Manitoba	85.8	1
4614	Division No. 14, Manitoba	85.1	1
4615	Division No. 15, Manitoba	80.7	2
4616	Division No. 16, Manitoba	73.2	4
4617	Division No. 17, Manitoba	77.2	3
4618	Division No. 18, Manitoba	72.0	4
4619	Division No. 19, Manitoba	46.9	5
4620	Division No. 20, Manitoba	75.3	3
4621	Division No. 21, Manitoba	73.2	4
4622	Division No. 22, Manitoba	55.4	5
4623	Division No. 23, Manitoba	49.6	5
4701	Division No. 1, Saskatchewan	82.7	2
4702	Division No. 2, Saskatchewan	85.2	1
4703	Division No. 3, Saskatchewan	83.1	2
4704	Division No. 4, Saskatchewan	83.4	2
4705	Division No. 5, Saskatchewan	84.5	2
4706	Division No. 6, Saskatchewan	89.8	1
4707	Division No. 7, Saskatchewan	84.8	2
4708	Division No. 8, Saskatchewan	86.2	1
4709	Division No. 9, Saskatchewan	82.9	2
4710	Division No. 10, Saskatchewan	79.9	3
4711	Division No. 11, Saskatchewan	90.2	1
4712	Division No. 12, Saskatchewan	82.0	2
4713	Division No. 13, Saskatchewan	81.6	2
4714	Division No. 14, Saskatchewan	80.5	2
4715	Division No. 15, Saskatchewan	83.0	2
4716	Division No. 16, Saskatchewan	76.1	3
4717	Division No. 17, Saskatchewan	77.5	3
4718	Division No. 18, Saskatchewan	51.3	5
4801	Division No. 1, Alberta	81.3	2
4802	Division No. 2, Alberta	81.5	2
4803	Division No. 3, Alberta	80.7	2
4804	Division No. 4, Alberta	85.2	1
4805	Division No. 5, Alberta	80.6	2
4806	Division No. 6, Alberta	90.0	1
4807	Division No. 7, Alberta	82.9	2
4808	Division No. 8, Alberta	83.2	2
4809	Division No. 9, Alberta	76.7	3
4810	Division No. 10, Alberta	81.2	2
4811	Division No. 11, Alberta	87.4	1
4812	Division No. 12, Alberta	80.1	2
4813	Division No. 13, Alberta	77.9	3
4814	Division No. 14, Alberta	77.5	3
4815	Division No. 15, Alberta	88.2	1
4816	Division No. 16, Alberta	90.1	1
4817	Division No. 17, Alberta	59.5	5
4818	Division No. 18, Alberta	71.2	4
4819	Division No. 19, Alberta	80.1	2
5901	East Kootenay, British Columbia	87.8	1
5903	Central Kootenay, British Columbia	85.9	1
5905	Kootenay Boundary, British Columbia	85.4	1
5907	Okanagan-Similkameen, British Columbia	84.0	2
5909	Fraser Valley, British Columbia	82.7	2
5915	Greater Vancouver, British Columbia	90.6	1
5917	Capital, British Columbia	90.6	1
5919	Cowichan Valley, British Columbia	83.5	2
5921	Nanaimo, British Columbia	86.7	1
5923	Alberni-Clayoquot, British Columbia	76.3	3
5924	Strathcona, British Columbia	83.7	2
5926	Comox Valley, British Columbia	87.2	1
5927	Powell River, British Columbia	82.8	2
5929	Sunshine Coast, British Columbia	86.8	1
5931	Squamish-Lillooet, British Columbia	90.6	1
5933	Thompson-Nicola, British Columbia	86.0	1
5935	Central Okanagan, British Columbia	88.7	1
5937	North Okanagan, British Columbia	85.0	1
5939	Columbia-Shuswap, British Columbia	84.2	2
5941	Cariboo, British Columbia	77.1	3
5943	Mount Waddington, British Columbia	70.3	4
5945	Central Coast, British Columbia	70.3	4
5947	Skeena-Queen Charlotte, British Columbia	72.5	4
5949	Kitimat-Stikine, British Columbia	76.3	3
5951	Bulkley-Nechako, British Columbia	78.0	3
5953	Fraser-Fort George, British Columbia	82.3	2
5955	Peace River, British Columbia	78.5	3
5957	Stikine, British Columbia	74.1	4
5959	Northern Rockies, British Columbia	74.6	4
6001	Yukon, Yukon	84.8	2
6101	Region 1, Northwest Territories	57.5	5
6102	Region 2, Northwest Territories	58.2	5
6103	Region 3, Northwest Territories	39.2	5
6104	Region 4, Northwest Territories	49.3	5
6105	Region 5, Northwest Territories	70.9	4
6106	Region 6, Northwest Territories	86.6	1
6204	Baffin, Nunavut	54.5	5
6205	Keewatin, Nunavut	40.7	5
6208	Kitikmeot, Nunavut	37.5	5
